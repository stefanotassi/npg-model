package com.ferrari.modis.dpg.util;

import java.security.NoSuchAlgorithmException;

public class CryptoHelper {
	public static String getHash(byte[] data, String hashType) throws NoSuchAlgorithmException {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance(hashType);
            byte[] array = md.digest(data);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
               sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static String md5(byte[] data) throws NoSuchAlgorithmException {
        return getHash(data, "MD5");
    }

    public static String sha1(byte[] data) throws NoSuchAlgorithmException {
        return getHash(data, "SHA1");
    }

}
