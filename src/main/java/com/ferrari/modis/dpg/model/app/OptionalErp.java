package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="CD_OPTIONALS_NPG_VW")

public class OptionalErp extends AuditableLang implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")		
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


 
	
	
}
