package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;





@Entity
@Table(name="MEDIA_TAG")

public class MediaTag extends Auditable implements Serializable {

	private static final long serialVersionUID = -2821137303417528305L;
	
	@EmbeddedId
	private MediaTagPK id;
	@Column(name="TAG", insertable = false,updatable = false)		
	private String tag;
	@Column(name="VAL")		
	private String val;
	public MediaTagPK getId() {
		return id;
	}
	public String getTag() {
		return tag;
	}
	public String getVal() {
		return val;
	}
	public void setId(MediaTagPK id) {
		this.id = id;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public void setVal(String val) {
		this.val = val;
	}
	
}

