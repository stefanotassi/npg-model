package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;


@Entity
@Table(name="OPTIONAL_TYPE_FORM")

public class OptionalTypeForm extends AuditableLang implements Serializable {

	private static final long serialVersionUID = 4458037885466266419L;

	@Id
	@Column(name="ID")	
	private Long id; // tabella gestita in backend

	@Column(name="ID_OPTIONAL_TYPE")		
	private String idOptionalType;
	
	@Column(name="ID_FORM")		
	private String idForm;

	@Column(name="SEQ")
	private Long sequence;
	
	@Column(name="ID_IMAGE")	
	private String idImage;
	
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;

	public Long getId() {
		return id;
	}

	public String getIdOptionalType() {
		return idOptionalType;
	}

	public String getIdForm() {
		return idForm;
	}

	public Long getSequence() {
		return sequence;
	}

	public String getIdImage() {
		return idImage;
	}

	public String getInternalNote() {
		return internalNote;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdOptionalType(String idOptionalType) {
		this.idOptionalType = idOptionalType;
	}

	public void setIdForm(String idForm) {
		this.idForm = idForm;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}

	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}



}
