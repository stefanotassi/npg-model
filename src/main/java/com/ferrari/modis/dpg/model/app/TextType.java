package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="TEXT_TYPES")

public class TextType extends AuditableLang implements Serializable {

	private static final long serialVersionUID = 3288541045491926246L;


	@Id
	@Column(name="ID")	
	private String id; 	

	
	// 
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}



	
	
}
