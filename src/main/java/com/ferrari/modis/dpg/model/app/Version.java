package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="VERSION")

public class Version extends AuditableLang implements Serializable {

	private static final long serialVersionUID = -2472232711017912355L;
	
	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Version-generator")
	@SequenceGenerator(name = "Version-generator", sequenceName = "SEQ_VERSION")
	private Long id; 	

	@Column(name="CODE")	
	private String code;
	@Column(name="ID_MODEL_OPTIONAL_TYPE")	
	private Long idModelOptionalType;
	@Column(name="STATUS")	
	private String status;
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;
	@Column( name = "DATE_PUB")
	@Temporal(TemporalType.TIMESTAMP)
	private Date publishDate;
	@Column( name = "USER_PUB" )
	private String publishUser;

	@Column(name="DESCRIPTION_LONG_IT")		
	private String descriptionLongIT;
	@Column(name="DESCRIPTION_LONG_EN")		
	private String descriptionLongEN;
	
	
	
	@OneToMany(mappedBy="version" ,cascade=CascadeType.ALL,orphanRemoval=true)
	private List<VersionAttc> lVersionAttc;
	
		
	public Version() {
		lVersionAttc = new ArrayList<VersionAttc>();
	}
	

		
 	public List<VersionAttc> getlVersionAttc() {
		return lVersionAttc;
	}
	public void setlVersionAttc(List<VersionAttc> lVersionAttc) {
		this.lVersionAttc = lVersionAttc;
	}
	public void addVersionAttc(VersionAttc VersionAttc) {
		if (lVersionAttc == null)
			lVersionAttc = new ArrayList<VersionAttc>();
		lVersionAttc.add(VersionAttc);
		VersionAttc.setVersion(this);
	}	
	 
	
	
	
	public Long getId() {
		return id;
	}
	public String getCode() {
		return code;
	}
	public Long getIdModelOptionalType() {
		return idModelOptionalType;
	}
	public String getStatus() {
		return status;
	}
	public String getInternalNote() {
		return internalNote;
	}
	public Date getPublishDate() {
		return publishDate;
	}
	public String getPublishUser() {
		return publishUser;
	}

	
	public void setId(Long id) {
		this.id = id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setIdModelOptionalType(Long idModelOptionalType) {
		this.idModelOptionalType = idModelOptionalType;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	public void setPublishUser(String publishUser) {
		this.publishUser = publishUser;
	}
	
	
	
	
	
	//////////////////////////////////////////////////////
	public String getDescriptionLong_it() {
		return descriptionLongIT;
	}
	public String getDescriptionLong_en() {
		return descriptionLongEN;
	}
	
	public String getDescriptionLong(String language ){
		switch (language) {
		case "it-IT":
			return this.descriptionLongIT;
		case "en-US":
			return this.descriptionLongEN;

		}
		return null;

	}
	
	
	public void setDescriptionLong_it(String descriptionLongIT) {
		this.descriptionLongIT = descriptionLongIT;
	}
	public void setDescriptionLong_en(String descriptionLongEN) {
		this.descriptionLongEN = descriptionLongEN;
	}
	
	public void setDescriptionLong(String description, String language)	{
		switch (language) {
		case "it-IT":
			this.descriptionLongIT = description;
			if (this.descriptionLongEN==null)
				this.descriptionLongEN = description;
			break;
		case "en-US":
			this.descriptionLongEN = description;
			if (this.descriptionLongIT==null)
				this.descriptionLongIT = description;
			break;
		}
		
		
	}
	
	
}
