package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;




@Entity
@Table(name="MEDIA_TYPE")

public class MediaType extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	
	@Column(name="TYPES")		
	private String types;

	@Column(name="MIN_X")		
	private Long minx;
	@Column(name="MIN_Y")		
	private Long miny;
	@Column(name="RATIO_X")		
	private Double ratiox;
	@Column(name="RATIO_Y")		
	private Double ratioy;
	
	@Column(name="ID_PLACEHOLDER")
	private String idPlaceholder;
	
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getTypes() {
		return types;
	}


	public void setTypes(String types) {
		this.types = types;
	}


	public Long getMinx() {
		return minx;
	}


	public void setMinx(Long minx) {
		this.minx = minx;
	}


	public Long getMiny() {
		return miny;
	}


	public void setMiny(Long miny) {
		this.miny = miny;
	}


	public Double getRatiox() {
		return ratiox;
	}


	public void setRatiox(Double ratiox) {
		this.ratiox = ratiox;
	}


	public Double getRatioy() {
		return ratioy;
	}


	public void setRatioy(Double ratioy) {
		this.ratioy = ratioy;
	}


	public String getInternalNote() {
		return internalNote;
	}


	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}


	public String getIdPlaceholder() {
		return idPlaceholder;
	}


	public void setIdPlaceholder(String idPlaceholder) {
		this.idPlaceholder = idPlaceholder;
	}
	
	
	
}

