package com.ferrari.modis.dpg.model.base;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class AuditContext {

	public static String getCurrentUserName() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null)
			return "DCambrea";
		else
			return auth.getName();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<String> getCurrentGroups() {
		List<String> groups = new ArrayList<String>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		List<GrantedAuthority> gGroups = null;
		if (auth == null) {
			gGroups = new ArrayList();
		} else
			gGroups = (List<GrantedAuthority>) auth.getAuthorities();
		// groups.add("*PUBLIC");
		for (GrantedAuthority g : gGroups) {
			groups.add(g.getAuthority());
		}

		return groups;
	}

	public static String getSignature() {
		List<String> groups = getCurrentGroups();
		for (String group : groups) {
			if (group.startsWith("#$"))
				return group;
		}
		return null;
	}

	public static Date getCurrentTimestamp() {
		Calendar c = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		return c.getTime();
	}

	public static boolean haveRole(String role) {
		return isInList(role, getCurrentGroups());

	}

	private static boolean isInList(String role, List<String> ars) {
		for (String s : ars) {
			if (role.equals(s))
				return true;
		}
		return false;
	}

	public boolean isUserInRole(String role) {
		return isGranted(role);
	}

	private boolean isGranted(String role) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if ((auth == null) || (auth.getPrincipal() == null)) {
			return false;
		}

		Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();

		if (authorities == null) {
			return false;
		}

		// This is the loop which do actual search
		for (GrantedAuthority grantedAuthority : authorities) {
			if (role.equals(grantedAuthority.getAuthority())) {
				return true;
			}
		}

		return false;
	}

	public static HttpServletRequest getCurrentRequest() {
		try {
			return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		} catch (Exception e) {
		}
		return null;
	}

//
	public static <T> T coalesce(T... items) {
		for (T i : items)
			if (i != null)
				return i;
		return null;
	}

	public static Boolean isActive(Auditable entity) {
		return entity == null ? Boolean.FALSE : "Y".equals(entity.getActive());
	}

	public static <T extends Auditable> List<T> filterActiveNewList(List<T> entities) {
		if (entities == null || entities.isEmpty())
			return new ArrayList<T>();
		List<T> ret = new ArrayList<T>();
		for (T i : ret) {
			if (isActive(i))
				ret.add(i);
		}
		return ret;
	}
	/**
	 * Copiato da ColletionUtils Apache commons
	 * @param <T>
	 * @param collection
	 * @return
	 */
	public static <T extends Auditable> boolean filterActive(final Iterable<T> collection) {
		boolean result = false;
		if (collection != null) {
			for (final Iterator<T> it = collection.iterator(); it.hasNext();) {
				if (!isActive(it.next())) {
					it.remove();
					result = true;
				}
			}
		}
		return result;
	}

}
