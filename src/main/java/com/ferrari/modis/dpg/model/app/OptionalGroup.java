package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;

// gestito in backend, P1, P2


@Entity
@Table(name="OPTIONAL_GROUP")

public class OptionalGroup extends AuditableLang implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "OptionalGroup-generator")
	@SequenceGenerator(name = "OptionalGroup-generator", sequenceName = "SEQ_OPTIONAL_GROUP")
	private Long id; // staccato da SEQ

	@Column(name="ID_OPTIONAL_TYPE")		
	private String idOptionalType;  // P1, ... P2
	@Column(name="CODE")		
	private String code;
	@Column(name="SEQ")		
	private Long sequence;
	
	public Long getId() {
		return id;
	}
	public String getIdOptionalType() {
		return idOptionalType;
	}
	public String getCode() {
		return code;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setIdOptionalType(String idOptionalType) {
		this.idOptionalType = idOptionalType;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	
}
