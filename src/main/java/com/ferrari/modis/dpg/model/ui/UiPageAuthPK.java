package com.ferrari.modis.dpg.model.ui;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UiPageAuthPK  implements Serializable{

	private static final long serialVersionUID = 5595989256132101050L;

	@Column(name="ID_PAGE")
	String idPage;

	@Column(name="ID_ACTION")
	String idAction;

	public String getIdPage() {
		return idPage;
	}

	public String getIdAction() {
		return idAction;
	}

	public void setIdPage(String idPage) {
		this.idPage = idPage;
	}

	public void setIdAction(String idAction) {
		this.idAction = idAction;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAction == null) ? 0 : idAction.hashCode());
		result = prime * result + ((idPage == null) ? 0 : idPage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UiPageAuthPK other = (UiPageAuthPK) obj;
		if (idAction == null) {
			if (other.idAction != null)
				return false;
		} else if (!idAction.equals(other.idAction))
			return false;
		if (idPage == null) {
			if (other.idPage != null)
				return false;
		} else if (!idPage.equals(other.idPage))
			return false;
		return true;
	}




}
