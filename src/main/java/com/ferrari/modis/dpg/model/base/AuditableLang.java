package com.ferrari.modis.dpg.model.base;


import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


@MappedSuperclass
public class AuditableLang extends Auditable{

	@Column(name="DESCRIPTION_IT")
	private String description_it;
	@Column(name="DESCRIPTION_EN")	
	private String description_en;
	
	
	public String getDescription_it() {
		return description_it;
	}
	public String getDescription_en() {
		return description_en;
	}
	
	public String getDescription(String language ){
		switch (language) {
		case "it-IT":
			return this.description_it;
		case "en-US":
			return this.description_en;

		}
		return null;

	}
	
	
	public void setDescription_it(String description_it) {
		this.description_it = description_it;
	}
	public void setDescription_en(String description_en) {
		this.description_en = description_en;
	}
	
	public void setDescription(String description, String language)	{
		switch (language) {
		case "it-IT":
			this.description_it = description;
			if (this.description_en==null)
				this.description_en = description;
			break;
			
		case "en-US":
			this.description_en = description;
			if (this.description_it==null)
				this.description_it = description;
			break;
		}
		
		
	}
	

}
