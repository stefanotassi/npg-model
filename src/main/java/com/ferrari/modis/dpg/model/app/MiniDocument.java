package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="DOCUMENT")

public class MiniDocument extends AuditableLang implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Model-generator")
	@SequenceGenerator(name = "Model-generator", sequenceName = "SEQ_DOCUMENT")
	private Long id; 	
	
	@Column(name="ID_DOCUMENT")	
	private String idDocument;
	@Column(name="VISIBLE")		
	private String visible;
	@Column(name="SEQ")
	private Long sequence;
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIdDocument() {
		return idDocument;
	}
	public void setIdDocument(String idDocument) {
		this.idDocument = idDocument;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public String getInternalNote() {
		return internalNote;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}



	
}
