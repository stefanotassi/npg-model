package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="VERSION_OPT_PICS")

public class VersionOptPics extends AuditableLang implements Serializable {

	private static final long serialVersionUID = -2472232711017912355L;
	
	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Version-opt-pics-generator")
	@SequenceGenerator(name = "Version-opt-pics-generator", sequenceName = "SEQ_VERSION_OPT_PICS")
	private Long id; 	

	

	@Column(name="PIC_TYPE") // principale-secondaria
	private String picType;
	
	@Column(name="ID_IMAGE")
	private String idImage;

	@Column(name="ID_IMAGE_LOW")
	private String idImageLow;
	
	@Column(name="SEQ") // sequenza di visualizzazione
	private Long sequence;
	
	
	
	@ManyToOne
	@JoinColumn(name="ID_VERSION_OPT")
	private VersionOpt versionOpt;

		
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getPicType() {
		return picType;
	}
	public void setPicType(String picType) {
		this.picType = picType;
	}
	public String getIdImage() {
		return idImage;
	}
	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}
	public VersionOpt getVersionOpt() {
		return versionOpt;
	}
	public void setVersionOpt(VersionOpt versionOpt) {
		this.versionOpt = versionOpt;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public String getIdImageLow() {
		return idImageLow;
	}
	public void setIdImageLow(String idImageLow) {
		this.idImageLow = idImageLow;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n\tVersionOptPics [id=");
		builder.append(id);
		builder.append(", picType=");
		builder.append(picType);
		builder.append(", idImage=");
		builder.append(idImage);
		builder.append(", idImageLow=");
		builder.append(idImageLow);
		builder.append(", sequence=");
		builder.append(sequence);

		builder.append("]");
		return builder.toString();
	}
	
}
