package com.ferrari.modis.dpg.model.ui;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;



@Entity
@Table(name="UI_APP_SETTINGS")
public class UiApplicationSetting extends Auditable implements Serializable{

	private static final long serialVersionUID = 6553898110556839586L;

	@Id
	@Column(name="key")
	String key;

	@Column(name="value")
	String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

    
}
