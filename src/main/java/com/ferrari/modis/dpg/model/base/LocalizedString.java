package com.ferrari.modis.dpg.model.base;


import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
final public class LocalizedString {
	

	
	@Column(name="LANGUAGE")
	 private String lang;

	@Column(name="DESCRIPTION")
    private String text;

	    public LocalizedString() {}

	    public LocalizedString(String language, String description) {
	        this.lang = language;
	        this.text = description;
	    }


		public String getLang() {
			return lang;
		}

		public void setLang(String lang) {
			this.lang = lang;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}
}
