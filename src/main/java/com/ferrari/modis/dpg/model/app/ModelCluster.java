package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;

// solo backend per la visualizzazione in lingua dei tre gruppi
// GT|SPORT|SPECIAL/LIMITED SERIES


@Entity
@Table(name="MODEL_CLUSTER")

public class ModelCluster extends AuditableLang implements Serializable {

	private static final long serialVersionUID = 5670151423596327812L;


	@Id
	private String id;


	@Column(name="SEQ")		
	private Long sequence;
	
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;
	
	
	
	
	public String getId() {
		return id;
	}

	public Long getSequence() {
		return sequence;
	}
	public String getInternalNote() {
		return internalNote;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}

	

	
}


