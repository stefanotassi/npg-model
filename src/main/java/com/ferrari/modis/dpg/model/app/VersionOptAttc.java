package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;


// un figurino puo' essere utilizzato da pi� optionals
// un optional pu� relazionare pi� figurini


@Entity
@Table(name="VERSION_OPT_ATTC")
public class VersionOptAttc extends Auditable  implements Serializable {

	private static final long serialVersionUID = -7451648448044042358L;
	
	@EmbeddedId
	VersionOptAttcPK id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_VERSION_OPT",insertable = false , updatable = false)
	VersionOpt versionOpt;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_VERSION_ATTC",insertable = false , updatable = false)	
	VersionAttc versionAttc;

	
	
	public VersionOptAttcPK getId() {
		return id;
	}
	public void setId(VersionOptAttcPK id) {
		this.id = id;
	}
	public VersionOpt getVersionOpt() {
		return versionOpt;
	}
	public void setVersionOpt(VersionOpt versionOpt) {
		this.versionOpt = versionOpt;
	}
	public VersionAttc getVersionAttc() {
		return versionAttc;
	}
	public void setVersionAttc(VersionAttc versionAttc) {
		this.versionAttc = versionAttc;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VersionOptAttc [id=");
		builder.append(id);
//		builder.append(", versionOpt=");
//		builder.append(versionOpt);
//		builder.append(", versionAttc=");
//		builder.append(versionAttc);
		builder.append("]");
		return builder.toString();
	}	
	
	@PreRemove
	public void remove() {
		this.versionOpt.getlVersionOptAttc().remove(this);
		this.versionAttc.getlVersionOptAttc().remove(this);
	}
}
