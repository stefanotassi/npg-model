package com.ferrari.modis.dpg.model.ui;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;

@Entity
@Table(name="UI_PAGES_AUTH")
public class UiPageAuth  extends Auditable {
	@EmbeddedId
	UiPageAuthPK id;
	
//	@ManyToOne
//	@JoinColumn(name="ID_PAGE",updatable=false,insertable=false)
//	UiPagegroup;
//
//	@ManyToOne
//	@JoinColumn(name="ID_ACTION",updatable=false,insertable=false)
//	UiSecAction action;




	public UiPageAuthPK getId() {
		return id;
	}

	public void setId(UiPageAuthPK id) {
		this.id = id;
	}

//	public UiSecGroup getGroup() {
//		return group;
//	}
//
//	public void setGroup(UiSecGroup group) {
//		this.group = group;
//	}
//
//	public UiSecAction getAction() {
//		return action;
//	}
//
//	public void setAction(UiSecAction action) {
//		this.action = action;
//	}





}
