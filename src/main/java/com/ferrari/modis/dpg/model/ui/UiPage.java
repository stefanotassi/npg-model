package com.ferrari.modis.dpg.model.ui;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;


@Entity
@Table(name="UI_PAGES")
public class UiPage extends Auditable{
	
	@Id
	@Column(name="id")
	String id;
	
	
	@Column(name="description")
	String description;

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	
}
