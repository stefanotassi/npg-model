package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="VERSION_OPT_TEXT")

public class VersionOptText extends AuditableLang implements Serializable {

	private static final long serialVersionUID = -2472232711017912355L;
	
	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Version-opt-text-generator")
	@SequenceGenerator(name = "Version-opt-text-generator", sequenceName = "SEQ_VERSION_OPT_TEXT")
	private Long id; 	
	
	
	@Column(name="TEXTTYPE") 
	private String textType;
	
	
	@ManyToOne
	@JoinColumn(name="ID_VERSION_OPT")
	private VersionOpt versionOpt;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	public String getTextType() {
		return textType;
	}
	public void setTextType(String textType) {
		this.textType = textType;
	}
	public VersionOpt getVersionOpt() {
		return versionOpt;
	}
	public void setVersionOpt(VersionOpt versionOpt) {
		this.versionOpt = versionOpt;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n\tVersionOptText [id=");
		builder.append(id);
		builder.append(", textType=");
		builder.append(textType);

		builder.append("]");
		return builder.toString();
	}

}
