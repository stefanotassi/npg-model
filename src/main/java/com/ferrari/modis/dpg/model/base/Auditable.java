package com.ferrari.modis.dpg.model.base;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@MappedSuperclass
public class Auditable{

	private static final String DEFAULT_ACTIVE = "Y";
	
	@ExcludeFromSave
	@Column( name = "ACTIVE" )
	private String active ;

	@ExcludeFromSave
	@Column( name = "DATE_INS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date insertTimestamp;

	@ExcludeFromSave
	@Column( name = "USER_INS" )
	private String insertUsername;

	@ExcludeFromSave
	@Column( name = "DATE_MOD")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateTimestamp;
	
	@ExcludeFromSave()
	@Column( name = "USER_MOD" )
	private String lastUpdateUsername;
	
	@PrePersist
    protected void onCreate() {
		insertUsername=AuditContext.getCurrentUserName();
		insertTimestamp = AuditContext.getCurrentTimestamp();
		active=DEFAULT_ACTIVE;
    }
//
    @PreUpdate
    protected void onUpdate() {
    	lastUpdateUsername=AuditContext.getCurrentUserName();
    	lastUpdateTimestamp = AuditContext.getCurrentTimestamp();
    }
	public String getActive() {
		return active==null ? DEFAULT_ACTIVE : active;
	}

	public void setActive(String active) {
		this.active = active==null ? DEFAULT_ACTIVE : active;
	}

	public Date getInsertTimestamp() {
		return this.insertTimestamp;
	}

	public void setInsertTimestamp(Date insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}

	public String getInsertUsername() {
		return this.insertUsername;
	}

	public void setInsertUsername( String insertUsername ) {
		this.insertUsername = insertUsername;
	}

	public Date getLastUpdateTimestamp() {
		return this.lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp( Date lastUpdateTimestamp ) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getLastUpdateUsername() {
		return this.lastUpdateUsername;
	}

	public void setLastUpdateUsername( String lastUpdateUsername ) {
		this.lastUpdateUsername = lastUpdateUsername;
	}

}
