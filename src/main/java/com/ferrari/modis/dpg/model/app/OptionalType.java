package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;

// gestito in backend, P1, P2


@Entity
@Table(name="OPTIONAL_TYPE")

public class OptionalType extends AuditableLang implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")		
	private String id; // P1, ... P2

	@Column(name="SEQ")		
	private Long sequence;
	
	@Column(name="VISIBLE")		
	private String visible;	

	@Column(name="ID_IMAGE")		
	private String idImage;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getIdImage() {
		return idImage;
	}

	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}	

	
	
}
