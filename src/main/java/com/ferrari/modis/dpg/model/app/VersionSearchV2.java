package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name="VERSION_SEARCH_V2_VW")

public class VersionSearchV2 implements Serializable {

	private static final long serialVersionUID = -9121640396807666283L;

	@Id
	@Column(name="PK")	
	private String pk; 	
	
	@Column(name="LANG")			
	private String language; 

	@Column(name="ID_VERSION")			
	private Long idVersion; 
	
	@Column(name="CODE_OPT")	
	private String codeOpt;
	
	@Column(name="ID_OPT")			
	private Long idOpt; 
	
	@Column(name="TB")	
	private String refTable;
	
	@Column(name="ID")			
	private Long idRefTable; 	
	
	@Column(name="DESCRIPTION")	
	private String description;
	
	@Column(name="URL")	
	private String url;

	@Column(name="PUB")	
	private String pub;

	@Column(name="VISOPT")	
	private String visible;
	
	
	
	@Column(name="VERSION_CODE")
	private String versionCode;
	@Column(name="VERSION_ID_MODEL_OPTIONAL_TYPE")
	private Long versionIdModelOptionalType;	
	@Column(name="VERSION_STATUS")
	private String versionStatus;	
	@Column(name="id_model")
	private Long idModel;	
	@Column(name="OPTIONAL_TYPE")
	private String optionalType;
	@Column(name="MODEL_DESCRIPTION")
	private String modelDescription;
	@Column(name="MODEL_VISIBLE")
	private String modelVisible;
	public String getPk() {
		return pk;
	}
	public void setPk(String pk) {
		this.pk = pk;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public Long getIdVersion() {
		return idVersion;
	}
	public void setIdVersion(Long idVersion) {
		this.idVersion = idVersion;
	}
	public String getCodeOpt() {
		return codeOpt;
	}
	public void setCodeOpt(String codeOpt) {
		this.codeOpt = codeOpt;
	}
	public Long getIdOpt() {
		return idOpt;
	}
	public void setIdOpt(Long idOpt) {
		this.idOpt = idOpt;
	}
	public String getRefTable() {
		return refTable;
	}
	public void setRefTable(String refTable) {
		this.refTable = refTable;
	}
	public Long getIdRefTable() {
		return idRefTable;
	}
	public void setIdRefTable(Long idRefTable) {
		this.idRefTable = idRefTable;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPub() {
		return pub;
	}
	public void setPub(String pub) {
		this.pub = pub;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	public String getVersionCode() {
		return versionCode;
	}
	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	public Long getVersionIdModelOptionalType() {
		return versionIdModelOptionalType;
	}
	public void setVersionIdModelOptionalType(Long versionIdModelOptionalType) {
		this.versionIdModelOptionalType = versionIdModelOptionalType;
	}
	public String getVersionStatus() {
		return versionStatus;
	}
	public void setVersionStatus(String versionStatus) {
		this.versionStatus = versionStatus;
	}
	public Long getIdModel() {
		return idModel;
	}
	public void setIdModel(Long idModel) {
		this.idModel = idModel;
	}
	public String getOptionalType() {
		return optionalType;
	}
	public void setOptionalType(String optionalType) {
		this.optionalType = optionalType;
	}
	public String getModelDescription() {
		return modelDescription;
	}
	public void setModelDescription(String modelDescription) {
		this.modelDescription = modelDescription;
	}
	public String getModelVisible() {
		return modelVisible;
	}
	public void setModelVisible(String modelVisible) {
		this.modelVisible = modelVisible;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VersionSearchV2 [pk=");
		builder.append(pk);
		builder.append(", language=");
		builder.append(language);
		builder.append(", idVersion=");
		builder.append(idVersion);
		builder.append(", codeOpt=");
		builder.append(codeOpt);
		builder.append(", idOpt=");
		builder.append(idOpt);
		builder.append(", refTable=");
		builder.append(refTable);
		builder.append(", idRefTable=");
		builder.append(idRefTable);
		builder.append(", description=");
		builder.append(description);
		builder.append(", url=");
		builder.append(url);
		builder.append(", pub=");
		builder.append(pub);
		builder.append(", visible=");
		builder.append(visible);
		builder.append(", versionCode=");
		builder.append(versionCode);
		builder.append(", versionIdModelOptionalType=");
		builder.append(versionIdModelOptionalType);
		builder.append(", versionStatus=");
		builder.append(versionStatus);
		builder.append(", idModel=");
		builder.append(idModel);
		builder.append(", optionalType=");
		builder.append(optionalType);
		builder.append(", modelDescription=");
		builder.append(modelDescription);
		builder.append(", modelVisible=");
		builder.append(modelVisible);
		builder.append("]");
		return builder.toString();
	}

	
	
	
}
