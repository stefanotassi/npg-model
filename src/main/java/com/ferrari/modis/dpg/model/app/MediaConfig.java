package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;




@Entity
@Table(name="MEDIA_CONFIG")
public class MediaConfig extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	
	@Column(name="IMAGE_P_COMPRESS_RATE")		
	private Double imagePCompressRate;

	@Column(name="IMAGE_P_RESIZE_FACTOR")	
	private Double imagePResizeFactor;
	

	@Column(name="IMAGE_S_COMPRESS_RATE")		
	private Double imageSCompressRate;
	@Column(name="IMAGE_S_RESIZE_FACTOR")		
	private Double imageSResizeFactor;

	
	public Double getImagePCompressRate() {
		return imagePCompressRate;
	}


	public void setImagePCompressRate(Double imagePCompressRate) {
		this.imagePCompressRate = imagePCompressRate;
	}


	public Double getImagePResizeFactor() {
		return imagePResizeFactor;
	}


	public void setImagePResizeFactor(Double imagePResizeFactor) {
		this.imagePResizeFactor = imagePResizeFactor;
	}


	public Double getImageSCompressRate() {
		return imageSCompressRate;
	}


	public void setImageSCompressRate(Double imageSCompressRate) {
		this.imageSCompressRate = imageSCompressRate;
	}


	public Double getImageSResizeFactor() {
		return imageSResizeFactor;
	}


	public void setImageSResizeFactor(Double imageSResizeFactor) {
		this.imageSResizeFactor = imageSResizeFactor;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	
	
}

