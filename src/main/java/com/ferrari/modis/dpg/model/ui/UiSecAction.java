package com.ferrari.modis.dpg.model.ui;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;



@Entity
@Table(name="UI_SEC_ACTIONS")
public class UiSecAction extends Auditable{
	@Id
	String id;
	
	@Column(name="NAME")
	String name;

	@Column(name="DESCRIPTION")
	String description;
	@Column(name="PERMIT")
	String perm;
	
	@OneToMany(mappedBy="action")
	private List<UiSecActionAuth> actions;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<UiSecActionAuth> getActions() {
		return actions;
	}

	public void setActions(List<UiSecActionAuth> actions) {
		this.actions = actions;
	}

	public String getPerm() {
		return perm;
	}

	public void setPerm(String perm) {
		this.perm = perm;
	}
	
}
