package com.ferrari.modis.dpg.model.ui;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;

@Entity
@Table(name="UI_DICTIONARY_LANG")

public class UiDictionaryLang extends Auditable implements Serializable {


	private static final long serialVersionUID = -1123393558123816411L;

	@Id
	private Long id;

	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="ID_DICTIONARY")
	private Long idDictionary;

	@Column(name="LANGUAGE")
	private String language;
	
	@ManyToOne
	@JoinColumn(name="ID_DICTIONARY",insertable = false , updatable = false)
	private UiDictionary uiDictionary;

	public UiDictionary getUiDictionary() {
		return uiDictionary;
	}

	public void setUiDictionary(UiDictionary uiDictionary) {
		this.uiDictionary = uiDictionary;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getIdDictionary() {
		return idDictionary;
	}

	public void setIdDictionary(Long idDictionary) {
		this.idDictionary = idDictionary;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}


}
