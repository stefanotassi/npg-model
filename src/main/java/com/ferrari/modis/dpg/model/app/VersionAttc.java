package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="VERSION_ATTC")

public class VersionAttc extends AuditableLang implements Serializable {

	private static final long serialVersionUID = -2472232711017912355L;
	
	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Version-attc-generator")
	@SequenceGenerator(name = "Version-attc-generator", sequenceName = "SEQ_VERSION_ATTC")
	private Long id; 	
	
	
	
	
	@Column(name="ATTC_TYPE")		
	private String attachmentType;
	
	@Column(name="ID_IMAGE")		
	private String idImage;
	
	@Column(name="ID_IMAGE_THUMB")		
	private String idImageThumb;

	@Column(name="ID_IMAGE_SVG")		
	private String idImageSvg;
	
	
	
	
	@Column(name="CONTENT_TYPE")		
	private String contentType;
	  
	@Column(name="SEQ")		
	private Long sequence;



	@Column(name="INTERNAL_NOTE")		
	private String internalNote;

  	@ManyToOne
	@JoinColumn(name="ID_VERSION")
	private Version version;

  	
  	@OneToMany(mappedBy="versionAttc"  ,cascade=CascadeType.ALL,orphanRemoval=true  ,fetch=FetchType.EAGER)
  	private List<VersionOptAttc> lVersionOptAttc;
  	

  	
	public VersionAttc() {
		lVersionOptAttc = new ArrayList<VersionOptAttc>();
	}

	public Version getVersion() {
		return version;
	}
	public void setVersion(Version version) {
		this.version = version;
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public String getAttachmentType() {
		return attachmentType;
	}

	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	public String getIdImage() {
		return idImage;
	}

	public void setIdImage(String idImage) {
		this.idImage = idImage;
	}

	public String getIdImageThumb() {
		return idImageThumb;
	}

	public void setIdImageThumb(String idImageThumb) {
		this.idImageThumb = idImageThumb;
	}

	public Long getSequence() {
		return sequence;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public String getInternalNote() {
		return internalNote;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}





 	public List<VersionOptAttc> getlVersionOptAttc() {
		return lVersionOptAttc;
	}
  	public void addVersionOptAttc(VersionOptAttc VersionOptAttc) {
		if (lVersionOptAttc == null)
			lVersionOptAttc = new ArrayList<VersionOptAttc>();
		lVersionOptAttc.add(VersionOptAttc);
		VersionOptAttc.setVersionAttc(this);
	}	
	public void setlVersionOptAttc(List<VersionOptAttc> lVersionOptAttc) {
		this.lVersionOptAttc = lVersionOptAttc;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getIdImageSvg() {
		return idImageSvg;
	}

	public void setIdImageSvg(String idImageSvg) {
		this.idImageSvg = idImageSvg;
	} 	
}
