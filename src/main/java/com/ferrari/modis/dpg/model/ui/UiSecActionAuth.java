package com.ferrari.modis.dpg.model.ui;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;

@Entity
@Table(name="UI_SEC_GROUPS_AUTH")
public class UiSecActionAuth extends Auditable {
	@EmbeddedId
	UiSecActionAuthPK id;
	
	@ManyToOne
	@JoinColumn(name="GROUP_ID",updatable=false,insertable=false)
	UiSecGroup group;

	@ManyToOne
	@JoinColumn(name="ACTION_ID",updatable=false,insertable=false)
	UiSecAction action;


	@Column(name="PERMIT")
	String perm;

	public UiSecActionAuthPK getId() {
		return id;
	}

	public void setId(UiSecActionAuthPK id) {
		this.id = id;
	}

	public UiSecGroup getGroup() {
		return group;
	}

	public void setGroup(UiSecGroup group) {
		this.group = group;
	}

	public UiSecAction getAction() {
		return action;
	}

	public void setAction(UiSecAction action) {
		this.action = action;
	}



	public String getPerm() {
		return perm;
	}

	public void setPerm(String perm) {
		this.perm = perm;
	}
	

}
