package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import com.ferrari.modis.dpg.model.base.Auditable;

@Entity
@Table(name="CFG_FILES_WHITELIST")
public class CfgFilesWhitelist extends Auditable implements Serializable{

	private static final long serialVersionUID = 2462008740630521320L;

	@Id
	@Column(name="ID")
	private Long id;

	@Column(name="MIME_TYPE")
	private String mimeType;

	@Column(name="EXTENSION")
	private String extension;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}


	
	
	
}
