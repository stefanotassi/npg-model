package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;



// tipi opt ammessi per modello, creato inizialmente automaticamente alla creazione del modello
// successivamente verr� creato scegliendo il tipo optional


// 1(meaningless) - 23(id model) - P1
// 2(meaningless) - 27(id model) - P1
// ....

@Entity
@Table(name="MODEL_OPTIONAL_TYPE")

public class ModelOptionalType extends Auditable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ModelOptionalType-generator")
	@SequenceGenerator(name = "ModelOptionalType-generator", sequenceName = "SEQ_MODEL_OPTIONAL_TYPE")
	private Long id; // staccato da SEQ
	
	@Column(name="ID_MODEL")		
	private Long idModel;

	@Column(name="ID_OPTIONAL_TYPE")		
	private String idOptionalType;
	
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;
	
	

	@OneToMany(mappedBy="modelOptionalType",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<ModelOptionalGroup> lModelOptionalGroup;
	
	  
	
	public ModelOptionalType() {
		lModelOptionalGroup = new ArrayList<ModelOptionalGroup>(); 
	}
 
	public Long getId() {
		return id;
	}

	public Long getIdModel() {
		return idModel;
	}

	public String getIdOptionalType() {
		return idOptionalType;
	}

	public String getInternalNote() {
		return internalNote;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdModel(Long idModel) {
		this.idModel = idModel;
	}

	public void setIdOptionalType(String idOptionalType) {
		this.idOptionalType = idOptionalType;
	}

	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}

	public List<ModelOptionalGroup> getlModelOptionalGroup() {
		return lModelOptionalGroup;
	}

	public void setlModelOptionalGroup(List<ModelOptionalGroup> lModelOptionalGroup) {
		this.lModelOptionalGroup = lModelOptionalGroup;
	}
	
	public void addlModelOptionalGroup(ModelOptionalGroup modelOptionalGroup) {

		if (lModelOptionalGroup == null)
			lModelOptionalGroup = new ArrayList<ModelOptionalGroup>();
		lModelOptionalGroup.add(modelOptionalGroup);
		modelOptionalGroup.setModelOptionalType(this);
		
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ModelOptionalType [id=");
		builder.append(id);
		builder.append(", idModel=");
		builder.append(idModel);
		builder.append(", idOptionalType=");
		builder.append(idOptionalType);
		builder.append(", internalNote=");
		builder.append(internalNote);
		builder.append(", lModelOptionalGroup=");
		builder.append(lModelOptionalGroup);
		builder.append("]");
		return builder.toString();
	}

}
