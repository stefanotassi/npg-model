package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name="VERSION_SEARCH_VW")

public class VersionSearch implements Serializable {

	private static final long serialVersionUID = -9121640396807666283L;

	@Id
	@Column(name="PK")	
	private String pk; 	
	
	@Column(name="LANG")			
	private String language; 

	@Column(name="ID_VERSION")			
	private Long idVersion; 
	
	@Column(name="CODE_OPT")	
	private String codeOpt;
	
	@Column(name="ID_OPT")			
	private Long idOpt; 
	
	@Column(name="TB")	
	private String refTable;
	
	@Column(name="ID")			
	private Long idRefTable; 	
	
	@Column(name="DESCRIPTION")	
	private String description;
	
	@Column(name="URL")	
	private String url;

	@Column(name="PUB")	
	private String pub;

	@Column(name="VISOPT")	
	private String visible;
	
	
	public String getPk() {
		return pk;
	}

	public void setPk(String pk) {
		this.pk = pk;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Long getIdVersion() {
		return idVersion;
	}

	public void setIdVersion(Long idVersion) {
		this.idVersion = idVersion;
	}

	public String getCodeOpt() {
		return codeOpt;
	}

	public void setCodeOpt(String codeOpt) {
		this.codeOpt = codeOpt;
	}

	public Long getIdOpt() {
		return idOpt;
	}

	public void setIdOpt(Long idOpt) {
		this.idOpt = idOpt;
	}

	public String getRefTable() {
		return refTable;
	}

	public void setRefTable(String refTable) {
		this.refTable = refTable;
	}

	public Long getIdRefTable() {
		return idRefTable;
	}

	public void setIdRefTable(Long idRefTable) {
		this.idRefTable = idRefTable;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VersionSearch [pk=");
		builder.append(pk);
		builder.append(", language=");
		builder.append(language);
		builder.append(", idVersion=");
		builder.append(idVersion);
		builder.append(", codeOpt=");
		builder.append(codeOpt);
		builder.append(", idOpt=");
		builder.append(idOpt);
		builder.append(", refTable=");
		builder.append(refTable);
		builder.append(", idRefTable=");
		builder.append(idRefTable);
		builder.append(", description=");
		builder.append(description);
		builder.append(", url=");
		builder.append(url);
		builder.append(", pub=");
		builder.append(pub);
		builder.append(", visible=");
		builder.append(visible);
		builder.append("]");
		return builder.toString();
	}

	public String getPub() {
		return pub;
	}

	public void setPub(String pub) {
		this.pub = pub;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}	
	
	
}
