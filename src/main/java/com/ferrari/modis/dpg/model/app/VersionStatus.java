package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="VERSION_STATUS")

public class VersionStatus extends AuditableLang implements Serializable {

	private static final long serialVersionUID = 8652065968281618512L;


	@Id
	@Column(name="ID")	
	private String id; 	
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;
	
	
	
	public String getId() {
		return id;
	}
	public String getInternalNote() {
		return internalNote;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}
}
