package com.ferrari.modis.dpg.model.ui;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;

@Entity
@Table(name="UI_DICTIONARY")

public class UiDictionary extends Auditable implements Serializable {

	private static final long serialVersionUID = -1123393558123816411L;

	@Id
	private Long id;

	private String description;

	@Column(name="ID_CONTEXT")
	private String idContext;

	@Column(name="ID_KEY")
	private String idKey;

	
	//@ElementCollection(fetch=FetchType.EAGER)
	 @OneToMany(mappedBy="uiDictionary")
	@MapKey(name = "language")
	 
	//@CollectionTable( name = "UI_DICTIONARY_LANG", joinColumns = @JoinColumn(name = "ID_DICTIONARY"))
	//@Transient
	private Map<String,UiDictionaryLang> descriptions;
	
	public UiDictionary() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdContext() {
		return this.idContext;
	}

	public void setIdContext(String idContext) {
		this.idContext = idContext;
	}

	public String getIdKey() {
		return this.idKey;
	}

	public void setIdKey(String idKey) {
		this.idKey = idKey;
	}

	public Map<String, UiDictionaryLang> getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(Map<String, UiDictionaryLang> descriptions) {
		this.descriptions = descriptions;
	}

}
