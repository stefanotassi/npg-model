package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

// un figurino puo' essere utilizzato da pi� optionals
// un optional pu� relazionare pi� figurini


@Embeddable
public class VersionOptAttcPK  implements Serializable {

	private static final long serialVersionUID = -4218877133871073439L;
	
	@Column(name="ID_VERSION_OPT" )
	Long IdVersionOpt;
	
	@Column(name="ID_VERSION_ATTC")
	Long IdVersionAttc;

	public Long getIdVersionOpt() {
		return IdVersionOpt;
	}

	public void setIdVersionOpt(Long idVersionOpt) {
		IdVersionOpt = idVersionOpt;
	}

	public Long getIdVersionAttc() {
		return IdVersionAttc;
	}

	public void setIdVersionAttc(Long idVersionAttc) {
		IdVersionAttc = idVersionAttc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IdVersionAttc == null) ? 0 : IdVersionAttc.hashCode());
		result = prime * result + ((IdVersionOpt == null) ? 0 : IdVersionOpt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VersionOptAttcPK other = (VersionOptAttcPK) obj;
		if (IdVersionAttc == null) {
			if (other.IdVersionAttc != null)
				return false;
		} else if (!IdVersionAttc.equals(other.IdVersionAttc))
			return false;
		if (IdVersionOpt == null) {
			if (other.IdVersionOpt != null)
				return false;
		} else if (!IdVersionOpt.equals(other.IdVersionOpt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VersionOptAttcPK [IdVersionOpt=");
		builder.append(IdVersionOpt);
		builder.append(", IdVersionAttc=");
		builder.append(IdVersionAttc);
		builder.append("]");
		return builder.toString();
	}

	
	
}
