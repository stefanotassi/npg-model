package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;


// per ogni id model_tipoopt --> elenco colonne Famiglie/Gruppi OPT
// 


@Entity
@Table(name="MODEL_OPT_SUBGROUP")

public class ModelOptionalSubGroup extends AuditableLang implements Serializable {

	private static final long serialVersionUID = 3494946305638076875L;

	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ModelOptionalGroup-generator")
	@SequenceGenerator(name = "ModelOptionalGroup-generator", sequenceName = "SEQ_MODEL_OPT_GROUP")
	private Long id; // staccato da SEQ
	

	@Column(name="CODE")		
	private String code;
	

	@Column(name="SEQ")		
	private Long sequence;

	@ManyToOne
	@JoinColumn(name="ID_MODEL_OPT_GROUP")
	private ModelOptionalGroup modelOptionalGroup;
		
	
	
	
	public Long getId() {
		return id;
	}



	public String getCode() {
		return code;
	}


	public Long getSequence() {
		return sequence;
	}


	public void setId(Long id) {
		this.id = id;
	}


//	public void setIdModelOptGroup(Long idModelOptGroup) {
//		this.idModelOptGroup = idModelOptGroup;
//	}


	public void setCode(String code) {
		this.code = code;
	}


	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}


	public ModelOptionalGroup getModelOptionalGroup() {
		return modelOptionalGroup;
	}


	public void setModelOptionalGroup(ModelOptionalGroup modelOptionalGroup) {
		this.modelOptionalGroup = modelOptionalGroup;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\n\tModelOptionalSubGroup [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", sequence=");
		builder.append(sequence);
//		builder.append(", modelOptionalGroup=");
//		builder.append(modelOptionalGroup);
		builder.append("]");
		return builder.toString();
	}


	
	

}
