package com.ferrari.modis.dpg.model.base;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ExcludeFromSave {
	 boolean _new() default true;
	 boolean _update() default true;
}
