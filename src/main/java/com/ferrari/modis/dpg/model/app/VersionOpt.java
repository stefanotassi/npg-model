package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="VERSION_OPT")

public class VersionOpt extends AuditableLang implements Serializable {

	private static final long serialVersionUID = -2472232711017912355L;
	
	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Version-opt-generator")
	@SequenceGenerator(name = "Version-opt-generator", sequenceName = "SEQ_VERSION_OPT")
	private Long id; 	
	
	@Column(name="ID_VERSION")			
	private Long idVersion; 
	
	@Column(name="CODE")	
	private String code;
	
	@Column(name="CODE_ERP")	
	private String codeErp;

	

	@Column(name="ID_MODEL_OPT_SUBGROUP")			
	private Long IdModelOptionalSubGroup; 
	
	

	
	@Column(name="VISIBLE")	
	private String visible;	
	
	@Column(name="ORDERABLE_IT")	
	private String orderable_it;	
	@Column(name="ORDERABLE_EN")	
	private String orderable_en;

	@Column(name="ORDERABLE")	
	private String orderableFlag;

	@Column(name="SEQ")		
	private Long sequence;

	@Column(name="VERSIONMODEL_IT")	
	private String versionModel_it;	
	@Column(name="VERSIONMODEL_EN")	
	private String versionModel_en;

	@Column(name="INTERNAL_NOTE")		
	private String internalNote;

	
	@OneToMany(mappedBy="versionOpt" ,cascade=CascadeType.ALL,orphanRemoval=true )
	private List<VersionOptPics> lVersionOptPics;
	@OneToMany(mappedBy="versionOpt" ,cascade=CascadeType.ALL,orphanRemoval=true )
	private List<VersionOptText> lVersionOptText;
  	@OneToMany(mappedBy="versionOpt" ,cascade=CascadeType.ALL,orphanRemoval=true )
  	private List<VersionOptAttc> lVersionOptAttc;
  	
	

	
	public VersionOpt() {
		lVersionOptPics = new ArrayList<VersionOptPics>();
		lVersionOptText = new ArrayList<VersionOptText>();
		lVersionOptAttc = new ArrayList<VersionOptAttc>();		
	}

	
 	public List<VersionOptAttc> getlVersionOptAttc() {
		return lVersionOptAttc;
	}
 
 	
	public void setlVersionOptAttr(List<VersionOptAttc> lVersionOptAttc) {
		this.lVersionOptAttc = lVersionOptAttc;
	}
  	public void addVersionOptAttc(VersionOptAttc VersionOptAttc) {
		if (lVersionOptAttc == null)
			lVersionOptAttc = new ArrayList<VersionOptAttc>();
		lVersionOptAttc.add(VersionOptAttc);
		VersionOptAttc.setVersionOpt(this);
	}	
	public void setlVersionOptAttc(List<VersionOptAttc> lVersionOptAttc) {
		this.lVersionOptAttc = lVersionOptAttc;
	} 	
	
	
  	
  	
  	
	
 	public List<VersionOptPics> getlVersionOptPics() {
		return lVersionOptPics;
	}
	public void setlVersionOptPics(List<VersionOptPics> lVersionOptPics) {
		this.lVersionOptPics = lVersionOptPics;
	}
  	public void addVersionOptPics(VersionOptPics VersionOptPics) {
		if (lVersionOptPics == null)
			lVersionOptPics = new ArrayList<VersionOptPics>();
		lVersionOptPics.add(VersionOptPics);
		VersionOptPics.setVersionOpt(this);
	}	
 
  	
  	
  	
  	
 	public List<VersionOptText> getlVersionOptText() {
		return lVersionOptText;
	}
	public void setlVersionOptText(List<VersionOptText> lVersionOptText) {
		this.lVersionOptText = lVersionOptText;
	}
  	public void addVersionOptText(VersionOptText VersionOptText) {
		if (lVersionOptText == null)
			lVersionOptText = new ArrayList<VersionOptText>();
		lVersionOptText.add(VersionOptText);
		VersionOptText.setVersionOpt(this);
	}	
	
	
	
	
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdVersion() {
		return idVersion;
	}

	public void setIdVersion(Long idVersion) {
		this.idVersion = idVersion;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Long getIdModelOptionalSubGroup() {
		return IdModelOptionalSubGroup;
	}

	public void setIdModelOptionalSubGroup(Long idModelOptionalSubGroup) {
		IdModelOptionalSubGroup = idModelOptionalSubGroup;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getOrderable_it() {
		return orderable_it;
	}

	public void setOrderable_it(String orderable_it) {
		this.orderable_it = orderable_it;
	}

	public String getOrderable_en() {
		return orderable_en;
	}

	public void setOrderable_en(String orderable_en) {
		this.orderable_en = orderable_en;
	}

	public String getOrderableFlag() {
		return orderableFlag;
	}

	public void setOrderableFlag(String orderableFlag) {
		this.orderableFlag = orderableFlag;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public String getVersionModel_it() {
		return versionModel_it;
	}

	public void setVersionModel_it(String versionModel_it) {
		this.versionModel_it = versionModel_it;
	}

	public String getVersionModel_en() {
		return versionModel_en;
	}

	public void setVersionModel_en(String versionModel_en) {
		this.versionModel_en = versionModel_en;
	}

	public String getInternalNote() {
		return internalNote;
	}

	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}
	
	
	
	public void setOrderable(String orderable, String language)	{
		switch (language) {
		case "it-IT":
			this.orderable_it = orderable;
			if (this.orderable_en==null)
				this.orderable_en = orderable;
			break;
		case "en-US":
			this.orderable_en = orderable;
			if (this.orderable_it==null)
				this.orderable_it = orderable;
			break;
		}
	}
	
 	public String getOrderable(String language ){
		switch (language) {
		case "it-IT":
			return this.orderable_it;
		case "en-US":
			return this.orderable_en;
		}
		return null;
	}	
 	public String getVersionModel(String language ){
		switch (language) {
		case "it-IT":
			return this.versionModel_it;
		case "en-US":
			return this.versionModel_en;
		}
		return null;
	}		

	public void setVersionModel(String versionModel, String language)	{
		switch (language) {
		case "it-IT":
			this.versionModel_it = versionModel;
			if (this.versionModel_en==null)
				this.versionModel_en = versionModel;
			break;
			
		case "en-US":
			this.versionModel_en = versionModel;
			if (this.versionModel_it==null)
				this.versionModel_it = versionModel;
			break;
		}
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VersionOpt [id=");
		builder.append(id);
		builder.append(", idVersion=");
		builder.append(idVersion);
		builder.append(", code=");
		builder.append(code);
		builder.append(", IdModelOptionalSubGroup=");
		builder.append(IdModelOptionalSubGroup);
		builder.append(", visible=");
		builder.append(visible);
		builder.append(", orderable_it=");
		builder.append(orderable_it);
		builder.append(", orderable_en=");
		builder.append(orderable_en);
		builder.append(", orderableFlag=");
		builder.append(orderableFlag);
		builder.append(", sequence=");
		builder.append(sequence);
		builder.append(", versionModel_it=");
		builder.append(versionModel_it);
		builder.append(", versionModel_en=");
		builder.append(versionModel_en);
		builder.append(", internalNote=");
		builder.append(internalNote);
		builder.append("\n, lVersionOptPics=");
		builder.append(lVersionOptPics);
		builder.append("\n, lVersionOptText=");
		builder.append(lVersionOptText);
		builder.append("\n, lVersionOptAttc=");
		builder.append(lVersionOptAttc);
		builder.append("]");
		return builder.toString();
	}


	public String getCodeErp() {
		return codeErp;
	}


	public void setCodeErp(String codeErp) {
		this.codeErp = codeErp;
	}




	
	
	
	

}
