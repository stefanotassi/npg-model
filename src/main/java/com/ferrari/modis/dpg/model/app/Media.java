package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.Auditable;




@Entity
@Table(name="MEDIA")

public class Media extends Auditable implements Serializable {

	private static final long serialVersionUID = -1517022879744610714L;

	@Id
	private String id;
	
	@Column(name="FILE_NAME")		
	private String filename;
	@Column(name="URL")		
	private String url;
	@Column(name="ID_MEDIA_TYPE")		
	private String idMediaType;
	@Column(name="WIDTH")		
	private Long width;
	@Column(name="HEIGHT")		
	private Long height;
	@Column(name="CONTENT_SIZE")		
	private Long contentSize;	
	@Column(name="CONTENT_TYPE")		
	private String contentType;	
	@Column(name="TRASHABLE")		
	private String trashable;	
	@Column(name="HASH_MD5")		
	private String hashmd5;	
	@Column(name="HOST_NAME_ORIG")		
	private String hostnameOrig;
	@Column(name="FILE_NAME_ORIG")		
	private String filenameOrig;
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;
	
	@Column(name="ID_THRON")		
	private String idThron;
	@Column(name="CLIENT_ID")		
	private String clienId;
	
	
	public String getClienId() {
		return clienId;
	}
	public void setClienId(String clienId) {
		this.clienId = clienId;
	}
	public String getId() {
		return id;
	}
	public String getFilename() {
		return filename;
	}
	public Long getWidth() {
		return width;
	}
	public Long getHeight() {
		return height;
	}
	public Long getContentSize() {
		return contentSize;
	}
	public String getContentType() {
		return contentType;
	}
	public String getTrashable() {
		return trashable;
	}
	public String getHashmd5() {
		return hashmd5;
	}
	public String getHostnameOrig() {
		return hostnameOrig;
	}
	public String getFilenameOrig() {
		return filenameOrig;
	}
	public String getInternalNote() {
		return internalNote;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public void setWidth(Long width) {
		this.width = width;
	}
	public void setHeight(Long height) {
		this.height = height;
	}
	public void setContentSize(Long contentSize) {
		this.contentSize = contentSize;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public void setTrashable(String trashable) {
		this.trashable = trashable;
	}
	public void setHashmd5(String hashmd5) {
		this.hashmd5 = hashmd5;
	}
	public void setHostnameOrig(String hostnameOrig) {
		this.hostnameOrig = hostnameOrig;
	}
	public void setFilenameOrig(String filenameOrig) {
		this.filenameOrig = filenameOrig;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}
	public String getIdMediaType() {
		return idMediaType;
	}
	public void setIdMediaType(String idMediaType) {
		this.idMediaType = idMediaType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIdThron() {
		return idThron;
	}
	public void setIdThron(String idThron) {
		this.idThron = idThron;
	}
	
}

