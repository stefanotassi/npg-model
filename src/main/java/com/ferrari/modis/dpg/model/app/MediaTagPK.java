package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;




@Embeddable
public class MediaTagPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ID_MEDIA")		
	private String idMedia;
	@Column(name="TAG")		
	private String tag;
	
	public MediaTagPK() {
	}

	public String getIdMedia() {
		return idMedia;
	}

	public String getTag() {
		return tag;
	}

	public void setIdMedia(String idMedia) {
		this.idMedia = idMedia;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idMedia == null) ? 0 : idMedia.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediaTagPK other = (MediaTagPK) obj;
		if (idMedia == null) {
			if (other.idMedia != null)
				return false;
		} else if (!idMedia.equals(other.idMedia))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}
	
}

