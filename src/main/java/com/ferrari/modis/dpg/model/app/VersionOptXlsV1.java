package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


// per creazione XLS semplice

@Entity
@Table(name="VERSION_OPT_V1_VW")

public class VersionOptXlsV1 implements Serializable {
	

	private static final long serialVersionUID = -2472232711017912355L;
	
	@EmbeddedId
	VersionOptXlsV1PK id;
	
	//////////////////////////////////////////////////////////
	@Column(name="MODEL_DESCRIPTION")	
	private String modelDescription;
	
	@Column(name="AREA")	
	private String area;
	@Column(name="DESC_GROUP")	
	private String groupDescription;	
	@Column(name="DESC_SUBGROUP")	
	private String subgroupDescription;

	@Column(name="OPT_CODE")	
	private String optCode;
	@Column(name="OPT_DESC")	
	private String optDescription;

	@Column(name="TEXT_A5_VINCOLI_OPT")
	private String textA5VincoliOpt;
	@Column(name="TEXT_B1_VINCOLO_OMOLOGAZIONE")
	private String textB1VincoliOmologazione;

	@Column(name="ORDERABLE_FLAG")
	private String orderableFlag;
	@Column(name="ORDERABLE_TEXT")
	private String orderableText;

	@Column(name="TEXT_A1_LONG")	
	private String textA1Long;
	
	@Column(name="VERSION_MODEL")		
	private String versionModel;

	@Column(name="LAST_UPDATE")		
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdate;
	
	@Column(name="TEXT_C0_NOTE_INTERNE")	
	private String textC0NoteInterne;
	@Column(name="TEXT_C2_STATUS_SVIL")	
	private String textC2StatusSvil;	
	@Column(name="TEXT_C4_CAPACITA")	
	private String textC4Capacity;	

	@Column(name="SEQUENCE_DISPLAY")	
	private Long seq;	

	@Column(name="VISIBLE_FLAG")	
	private String visibleFlag;

	@Column(name="SEQ_SUBGROUP")	
	private Long seqSubgroup;
	@Column(name="SEQ_GROUP")
	private Long seqGroup;
	
	@Column(name="ID_SUBGROUP")	
	private Long idSubgroup;
	@Column(name="ID_GROUP")
	private Long idGroup;
	

	@Column(name="CODE_SUBGROUP")	
	private String codSubgroup;
	@Column(name="CODE_GROUP")
	private String codGroup;

	
	@Column(name="IMG_P")
	private String idImageP;
	@Column(name="IMG_S1")
	private String idImageS1;
	@Column(name="IMG_S2")
	private String idImageS2;
	@Column(name="IMG_S3")
	private String idImageS3;
	@Column(name="IMG_S4")
	private String idImageS4;
	
	public String getModelDescription() {
		return modelDescription;
	}

	public void setModelDescription(String modelDescription) {
		this.modelDescription = modelDescription;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public String getSubgroupDescription() {
		return subgroupDescription;
	}

	public void setSubgroupDescription(String subgroupDescription) {
		this.subgroupDescription = subgroupDescription;
	}

	public String getOptCode() {
		return optCode;
	}

	public void setOptCode(String optCode) {
		this.optCode = optCode;
	}

	public String getOptDescription() {
		return optDescription;
	}

	public void setOptDescription(String optDescription) {
		this.optDescription = optDescription;
	}

	public String getTextA5VincoliOpt() {
		return textA5VincoliOpt;
	}

	public void setTextA5VincoliOpt(String textA5VincoliOpt) {
		this.textA5VincoliOpt = textA5VincoliOpt;
	}

	public String getTextB1VincoliOmologazione() {
		return textB1VincoliOmologazione;
	}

	public void setTextB1VincoliOmologazione(String textB1VincoliOmologazione) {
		this.textB1VincoliOmologazione = textB1VincoliOmologazione;
	}

	public String getOrderableFlag() {
		return orderableFlag;
	}

	public void setOrderableFlag(String orderableFlag) {
		this.orderableFlag = orderableFlag;
	}

	public String getOrderableText() {
		return orderableText;
	}

	public void setOrderableText(String orderableText) {
		this.orderableText = orderableText;
	}

	public String getTextA1Long() {
		return textA1Long;
	}

	public void setTextA1Long(String textA1Long) {
		this.textA1Long = textA1Long;
	}

	public String getVersionModel() {
		return versionModel;
	}

	public void setVersionModel(String versionModel) {
		this.versionModel = versionModel;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getTextC0NoteInterne() {
		return textC0NoteInterne;
	}

	public void setTextC0NoteInterne(String textC0NoteInterne) {
		this.textC0NoteInterne = textC0NoteInterne;
	}

	public String getTextC2StatusSvil() {
		return textC2StatusSvil;
	}

	public void setTextC2StatusSvil(String textC2StatusSvil) {
		this.textC2StatusSvil = textC2StatusSvil;
	}

	public String getTextC4Capacity() {
		return textC4Capacity;
	}

	public void setTextC4Capacity(String textC4Capacity) {
		this.textC4Capacity = textC4Capacity;
	}

	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}

	public String getVisibleFlag() {
		return visibleFlag;
	}

	public void setVisibleFlag(String visibleFlag) {
		this.visibleFlag = visibleFlag;
	}

	public VersionOptXlsV1PK getId() {
		return id;
	}

	public void setId(VersionOptXlsV1PK id) {
		this.id = id;
	}


	public Long getSeqSubgroup() {
		return seqSubgroup;
	}

	public void setSeqSubgroup(Long seqSubgroup) {
		this.seqSubgroup = seqSubgroup;
	}

	public Long getSeqGroup() {
		return seqGroup;
	}

	public void setSeqGroup(Long seqGroup) {
		this.seqGroup = seqGroup;
	}

	public Long getIdSubgroup() {
		return idSubgroup;
	}

	public void setIdSubgroup(Long idSubgroup) {
		this.idSubgroup = idSubgroup;
	}

	public Long getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(Long idGroup) {
		this.idGroup = idGroup;
	}

	public String getCodSubgroup() {
		return codSubgroup;
	}

	public void setCodSubgroup(String codSubgroup) {
		this.codSubgroup = codSubgroup;
	}

	public String getCodGroup() {
		return codGroup;
	}

	public void setCodGroup(String codGroup) {
		this.codGroup = codGroup;
	}

	public String getIdImageP() {
		return idImageP;
	}

	public void setIdImageP(String idImageP) {
		this.idImageP = idImageP;
	}

	public String getIdImageS1() {
		return idImageS1;
	}

	public void setIdImageS1(String idImageS1) {
		this.idImageS1 = idImageS1;
	}

	public String getIdImageS2() {
		return idImageS2;
	}

	public void setIdImageS2(String idImageS2) {
		this.idImageS2 = idImageS2;
	}

	public String getIdImageS3() {
		return idImageS3;
	}

	public void setIdImageS3(String idImageS3) {
		this.idImageS3 = idImageS3;
	}

	public String getIdImageS4() {
		return idImageS4;
	}

	public void setIdImageS4(String idImageS4) {
		this.idImageS4 = idImageS4;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("VersionOptXlsV1 [id=");
		builder.append(id);
		builder.append(", modelDescription=");
		builder.append(modelDescription);
		builder.append(", area=");
		builder.append(area);
		builder.append(", groupDescription=");
		builder.append(groupDescription);
		builder.append(", subgroupDescription=");
		builder.append(subgroupDescription);
		builder.append(", optCode=");
		builder.append(optCode);
		builder.append(", optDescription=");
		builder.append(optDescription);
		builder.append(", textA5VincoliOpt=");
		builder.append(textA5VincoliOpt);
		builder.append(", textB1VincoliOmologazione=");
		builder.append(textB1VincoliOmologazione);
		builder.append(", orderableFlag=");
		builder.append(orderableFlag);
		builder.append(", orderableText=");
		builder.append(orderableText);
		builder.append(", textA1Long=");
		builder.append(textA1Long);
		builder.append(", versionModel=");
		builder.append(versionModel);
		builder.append(", lastUpdate=");
		builder.append(lastUpdate);
		builder.append(", textC0NoteInterne=");
		builder.append(textC0NoteInterne);
		builder.append(", textC2StatusSvil=");
		builder.append(textC2StatusSvil);
		builder.append(", textC4Capacity=");
		builder.append(textC4Capacity);
		builder.append(", seq=");
		builder.append(seq);
		builder.append(", visibleFlag=");
		builder.append(visibleFlag);
		builder.append(", seqSubgroup=");
		builder.append(seqSubgroup);
		builder.append(", seqGroup=");
		builder.append(seqGroup);
		builder.append(", idSubgroup=");
		builder.append(idSubgroup);
		builder.append(", idGroup=");
		builder.append(idGroup);
		builder.append(", codSubgroup=");
		builder.append(codSubgroup);
		builder.append(", codGroup=");
		builder.append(codGroup);
		builder.append(", idImageP=");
		builder.append(idImageP);
		builder.append(", idImageS1=");
		builder.append(idImageS1);
		builder.append(", idImageS2=");
		builder.append(idImageS2);
		builder.append(", idImageS3=");
		builder.append(idImageS3);
		builder.append(", idImageS4=");
		builder.append(idImageS4);
		builder.append("]");
		return builder.toString();
	}	
	
}
