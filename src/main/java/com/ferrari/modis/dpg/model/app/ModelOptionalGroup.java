package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;


// per ogni id model_tipoopt --> elenco colonne Famiglie/Gruppi OPT
// 


@Entity
@Table(name="MODEL_OPT_GROUP")

public class ModelOptionalGroup extends AuditableLang implements Serializable {

	private static final long serialVersionUID = 3494946305638076875L;

	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ModelOptionalGroup-generator")
	@SequenceGenerator(name = "ModelOptionalGroup-generator", sequenceName = "SEQ_MODEL_OPT_GROUP")
	private Long id; // staccato da SEQ
	
	@Column(name="CODE")		
	private String code;
	
	@Column(name="SEQ")		
	private Long sequence;

	@ManyToOne
	@JoinColumn(name="ID_MODEL_OPTIONAL_TYPE")
	private ModelOptionalType modelOptionalType;
	

	@OneToMany(mappedBy="modelOptionalGroup" ,cascade=CascadeType.ALL,orphanRemoval=true)
	private List<ModelOptionalSubGroup> lModelOptionalSubGroup;
	

	
	public ModelOptionalGroup() {
		lModelOptionalSubGroup = new ArrayList<ModelOptionalSubGroup>();
	}

	public Long getId() {
		return id;
	}
	public String getCode() {
		return code;
	}
	public Long getSequence() {
		return sequence;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public ModelOptionalType getModelOptionalType() {
		return modelOptionalType;
	}
	public List<ModelOptionalSubGroup> getlModelOptionalSubGroup() {
		return lModelOptionalSubGroup;
	}
	public void setModelOptionalType(ModelOptionalType modelOptionalType) {
		this.modelOptionalType = modelOptionalType;
	}
	public void setlModelOptionalSubGroup(List<ModelOptionalSubGroup> lModelOptionalSubGroup) {
		this.lModelOptionalSubGroup = lModelOptionalSubGroup;
	}

  	public void addModelOptionalSubGroup(ModelOptionalSubGroup modelOptionalSubGroup) {

		if (lModelOptionalSubGroup == null)
			lModelOptionalSubGroup = new ArrayList<ModelOptionalSubGroup>();
		lModelOptionalSubGroup.add(modelOptionalSubGroup);
		modelOptionalSubGroup.setModelOptionalGroup(this);
		
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nModelOptionalGroup [id=");
		builder.append(id);
		builder.append(", code=");
		builder.append(code);
		builder.append(", sequence=");
		builder.append(sequence);
//		builder.append(", modelOptionalType=");
//		builder.append(modelOptionalType);
		builder.append(", lModelOptionalSubGroup=");
		builder.append(lModelOptionalSubGroup);
		builder.append("]");
		return builder.toString();
	}	



}
