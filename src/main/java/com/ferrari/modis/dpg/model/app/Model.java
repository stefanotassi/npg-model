package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ferrari.modis.dpg.model.base.AuditableLang;




@Entity
@Table(name="MODEL")

public class Model extends AuditableLang implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="ID")	
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "Model-generator")
	@SequenceGenerator(name = "Model-generator", sequenceName = "SEQ_MODEL")
	private Long id; 	
	
	
	@Column(name="CODE")	
	private String code;
	@Column(name="SEQ")
	private Long sequence;
	
	@Column(name="ID_IMAGE")	
	private String homepageImage;
	@Column(name="ID_IMAGE_LOGO")	
	private String logoImage;
	@Column(name="ID_IMAGE_COVER")	
	private String coverImage;
	
	
	@Column(name="ID_MCLUSTER")		
	private String idMcluster;
	@Column(name="VISIBLE")		
	private String visibility;
	@Column(name="INTERNAL_NOTE")		
	private String internalNote;
	
	@Column(name="ID_QUALITY")		
	private String idQuality;
	
	// 
	
	public String getIdQuality() {
		return idQuality;
	}
	public void setIdQuality(String idQuality) {
		this.idQuality = idQuality;
	}
	public Long getId() {
		return id;
	}
	public String getCode() {
		return code;
	}
	public Long getSequence() {
		return sequence;
	}
	public String getHomepageImage() {
		return homepageImage;
	}
	public String getIdMcluster() {
		return idMcluster;
	}
	public String getVisibility() {
		return visibility;
	}
	public String getInternalNote() {
		return internalNote;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}
	public void setHomepageImage(String homepageImage) {
		this.homepageImage = homepageImage;
	}
	public void setIdMcluster(String idMcluster) {
		this.idMcluster = idMcluster;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public void setInternalNote(String internalNote) {
		this.internalNote = internalNote;
	}
	public String getLogoImage() {
		return logoImage;
	}
	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}
	public String getCoverImage() {
		return coverImage;
	}
	public void setCoverImage(String coverImage) {
		this.coverImage = coverImage;
	}
	
	
	
}
