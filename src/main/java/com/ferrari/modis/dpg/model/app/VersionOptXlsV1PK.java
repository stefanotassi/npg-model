package com.ferrari.modis.dpg.model.app;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


// per creazione XLS semplice

@Embeddable
public class VersionOptXlsV1PK implements Serializable {
	
	private static final long serialVersionUID = -3492497247859250786L;

	@Column(name="ID_VERSION")			
	private Long idVersion; 
	
	@Column(name="ID_OPT")			
	private Long idOpt; 	
	@Column(name="LANGUAGE")			
	private String lang;
	public Long getIdVersion() {
		return idVersion;
	}
	public void setIdVersion(Long idVersion) {
		this.idVersion = idVersion;
	}
	public Long getIdOpt() {
		return idOpt;
	}
	public void setIdOpt(Long idOpt) {
		this.idOpt = idOpt;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	@Override
	public String toString() {
		return "VersionOptXlsV1PK [idVersion=" + idVersion + ", idOpt=" + idOpt + ", lang=" + lang + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idOpt == null) ? 0 : idOpt.hashCode());
		result = prime * result + ((idVersion == null) ? 0 : idVersion.hashCode());
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VersionOptXlsV1PK other = (VersionOptXlsV1PK) obj;
		if (idOpt == null) {
			if (other.idOpt != null)
				return false;
		} else if (!idOpt.equals(other.idOpt))
			return false;
		if (idVersion == null) {
			if (other.idVersion != null)
				return false;
		} else if (!idVersion.equals(other.idVersion))
			return false;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		return true;
	} 	
	
	
		
	
}
